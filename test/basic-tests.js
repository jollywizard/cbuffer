const assert = require('assert')

const CircularBuffer = require('..')

describe('Extended CBuffer', function() {

  it('new (no-arg)', function() {
    new CircularBuffer()
  })

  it('new (capacity)', function() {
    let cb = new CircularBuffer({capacity:2048})
    assert.equal(cb.size, 2048)
  })

  it('can push characters', function() {
    let cb = new CircularBuffer()
    for (let c of 'abc'.split(''))
      cb.push(c)

    assert.equal(cb.length,3)
    assert.equal(cb.toString(), 'abc')
  })

  it('can push string and empty', function() {
    let cb = new CircularBuffer()
    for (let c of 'abc')
      cb.push(c)

    assert.equal(cb.length,3)

    cb.empty()
    assert.equal(cb.length, 0)
  })

  it('can .write(strings)', function() {
    let cb = new CircularBuffer()
    cb.push('abc')

    assert.equal(cb.length,3)
    assert.equal(cb.toString(), 'abc')

    cb.push('def', 'ghi')
    assert.equal(cb.length, 9)
    assert.equal(cb.toString(), 'abcdefghi')
  })

  it('can .toBuffer(start,end)', function() {
    let cb = new CircularBuffer()
    cb.write('abc')

    let buff
    buff = cb.toBuffer({start:0,end:2})
    assert(buff instanceof Buffer)
    assert(buff.length = 2)
    assert.equal(buff.toString(),'ab')

    buff = cb.toBuffer({start:2})
    assert(buff instanceof Buffer)
    assert(buff.length = 1)
    assert.equal(buff.toString(), 'c')
  })

  it('can .shift(count)', function() {
    let cb = new CircularBuffer()
    cb.write('abc')

    let cut = cb.shift(2)

    assert.equal(cut.length, 2)
    assert.equal(cut.toString(),'ab')

    assert.equal(cb.length, 1)
    assert.equal(cb.toString(),'c')
  })

  it('can .unshift(strings)', function() {
    let cb = new CircularBuffer()
      , s1 = 'abc'
      , sa = ['a', 'bc', 'def']
    cb.write('abc')

    for (let s of sa)
      cb.unshift(s)

    let expected = sa.reverse().join('') + 'abc'
    let stringified = cb.toString()
    assert.equal(stringified, expected)
    assert.equal(cb.length, 9)
  })
})
