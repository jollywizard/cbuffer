# @ jamesarlow/cbuffer
Circular Node Buffer Class

## Overview

A modified [CBuffer](https://www.npmjs.com/package/CBuffer) which uses Buffer for storage, instead of Array.

## Installation

`npm install @jamesarlow/cbuffer`

## Usage

The module is the class. Use it with `new`.

See `CBuffer` for underlying methods.  This module class extends that one.

### Enhanced Methods

The following methods are enhanced to accept a variadic list of Strings, Buffers, and/or Arrays of byte value integers.

* push : add data to back of content
* unshift : add data to the front of content.

The following method is enhanced to accept a parameter that defines the width of the operation, and returns the result in a buffer if the width is greater than 1.

* shift : cut data from the front of content.

### Example

```
const CircularBuffer = require('@jw/cbuffer')
let cb = new CircularBuffer()

let symbolA = [240,157,156,156]
cb.push("Alpha", ' ', symbolA)

cb.toString()
// => 'alpha 𝜜'

cb.shift(5)
// => 'alpha'

cb.toString()
// =>  ' 𝜜'

cb.unshift('a')
cb.toString()
// => 'a 𝜜'
```

## Future plans

I reused the other module for a working baseline, but most of it can be brought together and simplified.  I will probably
merge the two code bases and remove the dependency.
