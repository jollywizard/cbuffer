const CBuffer = require('CBuffer')

/*
  A modification of npm package `CBuffer` targeting Buffer instead of array.

  The main logic of CBuffer is reused, thanks to Buffer's `buffer[index]` syntax.

  Unlike CBuffer, incoming data is iterated as bytes instead of being stored by reference.
  `.push(...)`, and `.unshift(...)` `.shift(int)` handle strings, buffers, and arrays,
  in addition to integers which are valid bytes.
*/
class CircularBuffer extends CBuffer {

  constructor({
    capacity:_capacity
  }={
    capacity:1024
  })
  {
    super(_capacity)
    const oldData = this.data
      ,   oldLen = oldData.length
    this.data = Buffer.alloc(oldData.length)
  }

  /**
    Sets the start value and resolves the modulus wrap-around.

    @param {int} the new start value to use.
    @return the clamped new value of start after the operation.
  */
  set start(newStart) {
    this._start = newStart % this.size
    return this._start
  }

  /**
    @return The start index for the content data.
  */
  get start() {
    return this._start
  }

  /**
    Removes bytes from the front of the content.

    @param {int} the number of bytes to shift. `default = 1`
  */
  shift(count) {
    if (!count)
      return super.shift()

    /* Just slice the whole return value for efficiency. */
    let r = this.toBuffer({start: 0, end: count})
    this.shiftStart(count)
    return r
  }

  /*
    Shrink the content from the front.
  */
  shiftStart(count)
  {
    this.start += count
    this.length -= count
  }

  /**
    Append data to the front end of the content.

    Each parameter can be a string, number, buffer, or array.

    Numbers are treated as bytes.

    Strings, buffers, and arrays are iterated.
   */
  unshift(...vals) {
    for (let v of vals)
    {
      if (typeof v == 'string')
      {
        super.unshift(...Buffer.from(reverse(v)))
      }
      else if (Buffer.isBuffer(v) || v instanceof Array)
      {
        super.unshift(...v.reverse())
      }
      else
      {
        super.unshift(v)
      }
    }
  }


  /*
    Append data to the back end of content.

    Each parameter can be a string, number, buffer, or array.

    Numbers are treated as bytes.

    Strings, buffers, and arrays are iterated.
  */
  push(...vals)
  {
    for (let v of vals)
    {
      if (typeof v == 'string')
      {
        super.push(...Buffer.from(v))
      }
      else if (Buffer.isBuffer(v) || v instanceof Array)
      {
        super.push(...v)
      }
      else if (!isNaN(v))
      {
        super.push(v)
      }
    }
  }

  /**
    @return {String} the content as UTF-8.
  */
  toString( opts = {start:0, end: this.length})
  {
    if (!opts.start) opts.start = 0
    if (!opts.end) opts.end = this.length
    if (!('encoding' in opts)) opts.encoding = 'utf8'
    let bufferString =
      this.toBuffer({start:opts.start,end:opts.end})
          .toString(opts.encoding)
    return `${bufferString}`
  }

  /**
   Get the content area (or slice) as a buffer.
  **/
  toBuffer({start: start, end: end} = {start:0, end:this.length}) {
    let slice = this.slice(start, end)
    return Buffer.from(slice)
  }

  /**
    Get a Writable Stream that appends to the content area.
  */
  get writable() {
    return new Writable(this)
  }

  /**
    An alias for `push(string)`.
  */
  write(string)
  {
    this.push(string)
  }
}

class Writable extends require('stream').Writable
{
  constructor(cbuffer)
  {
    super()
    this.cbuffer = cbuffer
  }

  _write(data, enc, cb)
  {
    this.cbuffer.push(data)
    cb()
  }
}

function reverse(string)
{
  return string.split('').reverse().join('')
}

module.exports = CircularBuffer
